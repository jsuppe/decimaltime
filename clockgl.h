#ifndef CLOCKGL_H
#define CLOCKGL_H

#include <QOpenGLWidget>

class ClockGLPrivate;
class ClockGL : public QOpenGLWidget
{
public:
    ClockGL(QWidget *parent);
    ~ClockGL();

protected:
    virtual void initializeGL();
    virtual void paintGL();
    virtual void resizeGL();

private:
    ClockGLPrivate *d;
};

#endif // CLOCKGL_H

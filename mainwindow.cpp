#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDateTime>
#include <math.h>
#include "clock.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    m_clock(NULL)
{
    ui->setupUi(this);

    // This is default zero rotation for this dial so that 0 is pointing right
    m_zeroRotation = ui->dial->value();
    m_initialDialMax = ui->dial->maximum();

    TimeFormat format;
    format.cyclesPerDay    = ui->doubleSpinBoxCycles->value();
    format.hoursPerCycle   = ui->doubleSpinBoxHours->value();
    format.minutesPerHour  = ui->doubleSpinBoxMinutes->value();
    format.secondPerMinute = ui->doubleSpinBoxSeconds->value();
    format.rotateClockwise = ui->checkBoxRotateClockwise->isChecked();

    m_clock = new Clock(format,this);

    m_timer.setInterval(ui->doubleSpinBoxFrequency->value());

    bool ok = false;
    connect(&m_timer, &QTimer::timeout, this, &MainWindow::onTimeout);
    connect(&m_timer, &QTimer::timeout, this, &MainWindow::onTimeout2);
    connect(&m_timer, &QTimer::timeout, m_clock, &Clock::updateTime);

    m_timer.start();

    ui->frameDecimalClock->layout()->addWidget(m_clock);

}

void MainWindow::onTimeout(){

    ClockTime ct = m_clock->time();

    ui->labelHour  ->setText(QString::number(static_cast<int>(floor(ct.hour))));
    ui->labelMinute->setText(QString::number(static_cast<int>(floor(ct.minute))));
    ui->labelSecond->setText(QString::number(static_cast<int>(floor(ct.second))));
}

void MainWindow::onTimeout2(){
    int h = QDateTime::currentDateTime().time().hour();
    int m = QDateTime::currentDateTime().time().minute();
    int s = QDateTime::currentDateTime().time().second();


    ui->labelH->setText(QString::number(h));
    ui->labelM->setText(QString::number(m));
    ui->labelS->setText(QString::number(s));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_doubleSpinBoxCycles_valueChanged(double arg1)
{
    TimeFormat format = m_clock->format();
    format.cyclesPerDay = arg1;
    m_clock->setFormat(format);
}

void MainWindow::on_doubleSpinBoxHours_valueChanged(double arg1)
{
    TimeFormat format = m_clock->format();
    format.hoursPerCycle = arg1;
    m_clock->setFormat(format);
}

void MainWindow::on_doubleSpinBoxMinutes_valueChanged(double arg1)
{
    TimeFormat format = m_clock->format();
    format.minutesPerHour = arg1;
    m_clock->setFormat(format);
}

void MainWindow::on_doubleSpinBoxSeconds_valueChanged(double arg1)
{
    TimeFormat format = m_clock->format();
    format.secondPerMinute = arg1;
    m_clock->setFormat(format);
}

void MainWindow::on_dial_valueChanged(int value)
{
    ui->labelDialValue->setText(QString::number(value));

    TimeFormat format = m_clock->format();
    format.orientation = value - m_initialDialMax;
    double zeroDegrees = 360 * m_zeroRotation / m_initialDialMax;
    format.orientation = (360 / ui->dial->maximum() * value)  - zeroDegrees;
    m_clock->setFormat(format);
}

void MainWindow::on_checkBoxRotateClockwise_clicked(bool checked)
{
    TimeFormat format = m_clock->format();
    format.rotateClockwise = checked;
    m_clock->setFormat(format);
}

void MainWindow::on_doubleSpinBoxFrequency_valueChanged(double arg1)
{
    if(arg1 > 0)
        this->m_timer.setInterval(arg1);
}

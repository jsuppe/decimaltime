#include "clockgl.h"
#include <QOpenGLFunctions>

class ClockGLPrivate : public QOpenGLFunctions {

public:
    ClockGLPrivate(ClockGL *owner);
    void doInitializeGL();
    void doPaintGL();
    void doResizeGL();

private:
    ClockGL *m_Owner;
};

ClockGLPrivate::ClockGLPrivate(ClockGL *owner):
    m_Owner(owner){

}

void ClockGLPrivate::doInitializeGL(){
    initializeOpenGLFunctions();

    glClearColor(1.0f, 0.0f, 0.0f, 1.0f);
    glClear(GL_COLOR);
}

void ClockGLPrivate::doPaintGL(){
    glClear(GL_COLOR);
}

void ClockGLPrivate::doResizeGL(){

}

ClockGL::ClockGL(QWidget *parent):
    QOpenGLWidget(parent),
    d(new ClockGLPrivate(this))
{

}

ClockGL::~ClockGL()
{
    delete d;
}

void ClockGL::initializeGL()
{
    d->doInitializeGL();
}

void ClockGL::paintGL()
{
    d->doPaintGL();
}

void ClockGL::resizeGL()
{
    d->doResizeGL();
}

#ifndef CLOCK_H
#define CLOCK_H

#include <QWidget>
#include <QList>

class QGraphicsLineItem;
class QGraphicsEllipseItem;
class QGraphicsView;
class QTimer;
class QGraphicsTextItem;

struct ClockTime {
    double hour;
    double minute;
    double second;
    double millisecond;
};

struct TimeFormat {
    double cyclesPerDay;
    double hoursPerCycle;
    double minutesPerHour;
    double secondPerMinute;
    bool   rotateClockwise;
    double orientation;
};

class Clock : public QWidget
{
    Q_OBJECT
public:
    explicit Clock(TimeFormat dt, QWidget *parent = 0);

    TimeFormat format();
    void setFormat(TimeFormat);
    ClockTime time();
signals:

public slots:
    void updateTime();

protected:
    void initNumbers();
    void initClock();
    void initItems();

    virtual void resizeEvent(QResizeEvent *event);

protected:
    void resetScene();

private:
    QList<QGraphicsTextItem *> m_numbers;
    QGraphicsView        *m_view;
    QGraphicsLineItem    *m_msec;
    QGraphicsLineItem    *m_second;
    QGraphicsLineItem    *m_minute;
    QGraphicsLineItem    *m_hour;
    QGraphicsEllipseItem *m_circle;

    TimeFormat m_timeFormat;

    qreal m_dmsecPermsec;
    QTimer *m_timer;
};

#endif // CLOCK_H

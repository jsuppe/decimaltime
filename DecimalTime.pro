#-------------------------------------------------
#
# Project created by QtCreator 2016-07-28T22:37:17
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = DecimalTime
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    clock.cpp \
    clockgl.cpp

HEADERS  += mainwindow.h \
    clock.h \
    clockgl.h

FORMS    += mainwindow.ui

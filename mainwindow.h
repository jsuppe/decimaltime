#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTimer>

namespace Ui {
class MainWindow;
}

class Clock;
class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

public slots:
    void onTimeout2();
    void onTimeout();

private slots:
    void on_doubleSpinBoxCycles_valueChanged(double arg1);

    void on_doubleSpinBoxHours_valueChanged(double arg1);

    void on_doubleSpinBoxMinutes_valueChanged(double arg1);

    void on_doubleSpinBoxSeconds_valueChanged(double arg1);

    void on_dial_valueChanged(int value);


    void on_checkBoxRotateClockwise_clicked(bool checked);

    void on_doubleSpinBoxFrequency_valueChanged(double arg1);

private:
    qreal m_initialDialMax;
    qreal m_zeroRotation;
    qreal m_dmsecPermsec;
    Ui::MainWindow *ui;
    QTimer m_timer;
    Clock *m_clock;
};

#endif // MAINWINDOW_H

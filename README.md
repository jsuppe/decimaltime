# README #

Decimal Time is a demo app for calculating and displaying decimal time. In this clock there are 10 Hours per day, 100 minutes per hour, 100 seconds per minute. I've also created the analog to rotate counter-clock wise and start at 0 degrees which to me makes reading it much more intuitive.


## Why ##
Motivation for this project was from repost of a [reddit link](https://www.reddit.com/r/todayilearned/comments/1k8o90/til_france_once_tried_simplifying_time_by_using_a/?st=irec8p82&sh=1d86b752) mentioning old french decimal clocks. 



I wanted to see what a 100 second minute "felt" like so I wanted to see it tick. Started with text then wanted to see analog so I added that
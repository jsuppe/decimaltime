#include "clock.h"
#include <QGraphicsView>
#include <QVBoxLayout>
#include <QGraphicsScene>
#include <QGraphicsEllipseItem>
#include <QTimer>
#include <QDateTime>
#include <math.h>
#include <QLine>

Clock::Clock(TimeFormat dt, QWidget *parent) : QWidget(parent),m_view(new QGraphicsView(this))
{
    setLayout(new QVBoxLayout());
    layout()->addWidget(m_view);

    m_view->setScene(new QGraphicsScene(this));
    m_view->scene()->setSceneRect(0,0,100,100);

    setFormat(dt);
    resetScene();
}

TimeFormat Clock::format()
{
    return m_timeFormat;
}

void Clock::setFormat(TimeFormat dt){
    m_timeFormat = dt;
    resetScene();
}

void Clock::initClock(){
    qreal msecondsPerDay = 24 * 60 * 60 * 1000;
    qreal dmsecondsPerDay =
            m_timeFormat.cyclesPerDay *
            m_timeFormat.hoursPerCycle *
            m_timeFormat.minutesPerHour *
            m_timeFormat.secondPerMinute * 1000;

    m_dmsecPermsec = dmsecondsPerDay / msecondsPerDay;
}

void Clock::initItems(){
    QRectF srect = m_view->sceneRect();
    QPen msecPen;
    QPen secondPen;
    QPen minutePen;
    QPen hourPen;
    QPen circlePen;

    m_msec   = new QGraphicsLineItem();
    m_second = new QGraphicsLineItem();
    m_minute = new QGraphicsLineItem();
    m_hour   = new QGraphicsLineItem();
    m_circle = new QGraphicsEllipseItem(0,0,srect.width(),srect.height());

    msecPen.setColor(QColor(Qt::white));
    msecPen.setWidth(1);
    secondPen.setColor(QColor(Qt::green));
    secondPen.setWidth(1);
    minutePen.setColor(QColor(Qt::cyan));
    minutePen.setWidth(3);
    hourPen.setColor(QColor(Qt::gray));
    hourPen.setWidth(5);
    circlePen.setColor(QColor(Qt::red));
    circlePen.setWidth(5);

    m_msec  ->setPen(msecPen);
    m_second->setPen(secondPen);
    m_minute->setPen(minutePen);
    m_hour  ->setPen(hourPen);
    m_circle->setPen(circlePen);

    m_view->scene()->addItem(m_circle);
    m_view->scene()->addItem(m_hour  );
    m_view->scene()->addItem(m_minute);
    m_view->scene()->addItem(m_second);
    m_view->scene()->addItem(m_msec  );

    m_view->scene()->setBackgroundBrush(QBrush(QColor(Qt::darkCyan)));
}

void Clock::resizeEvent(QResizeEvent *event)
{
    QWidget::resizeEvent(event);
    QRect viewRect = m_view->rect();
    qreal dim = qMin(viewRect.width(),viewRect.height());
    QRect newSceneRect = QRect(0,0,dim,dim);
    m_view->scene()->setSceneRect(newSceneRect);

    resetScene();
}

void Clock::resetScene(){
    // Clear old numbers
    foreach(QGraphicsTextItem *number, m_numbers){
        number->deleteLater();
    }
    m_numbers.clear();

    m_view->scene()->clear();

    initClock();
    initItems();
    initNumbers();
}

void Clock::initNumbers(){

    QRectF srect = m_view->sceneRect();
    QTransform tr;

    int mul = m_timeFormat.rotateClockwise ? 1 : -1;

    for(int hour = 1; hour <= m_timeFormat.hoursPerCycle; hour++ ){
        QLine line = QLine(QPoint(0,0),QPoint(srect.width() / 2.3, 0));
        tr.reset();
        QGraphicsTextItem *text = new QGraphicsTextItem(QString::number(hour));
        tr.translate(
                    srect.width()/2.0 -
                text->boundingRect().width()/2.0,
                     srect.height()/2.0 -
                text->boundingRect().height()/2.0
                    );
        tr.rotate( m_timeFormat.orientation + (360.0 / m_timeFormat.hoursPerCycle * hour) * mul);
        line = tr.map(line);
        text->setPos(line.p2());
        m_view->scene()->addItem(text);
        m_numbers << text;
    }
}

ClockTime Clock::time(){
    int hour = QDateTime::currentDateTime().time().hour();
    int min  = QDateTime::currentDateTime().time().minute();
    int sec  = QDateTime::currentDateTime().time().second();
    int msec = QDateTime::currentDateTime().time().msec();

    qreal rat = (hour + min / 60.0 + sec / 3600.0 + msec / 3600000.00) / 24.0;

    double dtime = rat * m_timeFormat.hoursPerCycle * m_timeFormat.cyclesPerDay;

    double h = dtime;
    double m = (dtime - floor(dtime)) * m_timeFormat.minutesPerHour;
    double s = (m - floor(m)) * m_timeFormat.secondPerMinute;
    double ms = (s - floor(s)) * 1000;

    ClockTime ct;
    ct.hour        = h;
    ct.minute      = m;
    ct.second      = s;
    ct.millisecond = ms;

    return ct;
}

void Clock::updateTime(){

    QRectF srect = m_view->sceneRect();

    ClockTime ct = this->time();

    QLine msl = QLine(QPoint(0,0),QPoint(srect.width()/6,0));
    QLine sl = QLine(QPoint(0,0),QPoint(srect.width()/2,0));
    QLine ml = QLine(QPoint(0,0),QPoint(srect.width()/2.5,0));
    QLine hl = QLine(QPoint(0,0),QPoint(srect.width()/3,0));

    QTransform msr,sr,mr,hr;

    int mul = m_timeFormat.rotateClockwise ? 1 : -1;

    msr.translate(srect.width()/2,srect.height()/2);
    msr.rotate( m_timeFormat.orientation +  (360.0 / 1000 *  floor(ct.millisecond))  * mul);

    sr.translate(srect.width()/2,srect.height()/2);
    sr.rotate( m_timeFormat.orientation +  (360.0 / m_timeFormat.secondPerMinute *  floor(ct.second)) * mul);

    mr.translate(srect.width()/2,srect.height()/2);
    mr.rotate( m_timeFormat.orientation +  (360.0 / m_timeFormat.minutesPerHour * floor(ct.minute)) * mul);

    hr.translate(srect.width()/2,srect.height()/2);
    hr.rotate( m_timeFormat.orientation +  (360.0 / m_timeFormat.hoursPerCycle * floor(ct.hour)) * mul);

    msl = msr.map(msl);
    sl = sr.map(sl);
    ml = mr.map(ml);
    hl = hr.map(hl);

    m_msec   ->setLine(msl);
    m_second ->setLine(sl);
    m_minute ->setLine(ml);
    m_hour   ->setLine(hl);
    m_circle ->setRect(0,0,srect.width(),srect.height());

    update();
}
